import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css';
import Vue from 'vue'


import Sidebar from './components/navigation/SideBar.vue'

Vue.use(Sidebar)

import { router } from './assets/js/routes'

import "chart.js"
import "hchs-vue-charts"

Vue.use(window.VueCharts);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(Sidebar),
}).$mount('#app')
