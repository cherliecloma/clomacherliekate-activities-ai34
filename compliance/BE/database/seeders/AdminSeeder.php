<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminkey = "admin123";
        $status = "lock";

        DB::table('admin')->insert([
            'adminkey' => $adminkey,
            'status' => $status,
        ]);
    }
}
