<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricePerKilo extends Model
{
    use HasFactory;
    protected $table = "priceperkilo";
    protected $fillable = [
        'pid',
        'price',
    ];
}
