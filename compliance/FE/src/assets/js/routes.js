import Vue from 'vue'
import VueRouter from 'vue-router'

import Dashboard from '../../components/pages/admin/Dashboard'
import NewTransaction from '../../components/pages/admin/NewTransaction'
import PriceAndOffer from '../../components/pages/admin/PriceAndOffer'
import Transactions from '../../components/pages/admin/Transactions'
import Customer from '../../components/pages/customer/Customer'

//for router
Vue.use(VueRouter)

export const router = new VueRouter({
    routes: [
        { path: '/', component: Dashboard },
        { path: '/index', component: Dashboard },
        { path: '/newtransaction', component: NewTransaction },
        { path: '/priceandoffer', component: PriceAndOffer },
        { path: '/transactions', component: Transactions },
        { path: '/customer', component: Customer },
    ]
  })
  