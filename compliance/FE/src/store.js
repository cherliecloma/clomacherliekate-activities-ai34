import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';

Vue.use(Vuex, axios);

export default new Vuex.Store({
    state: {
        searchdata: {
            name: '',
            weight: '',
            total: '',
            amount: '',
            change: ''
        },
        admindata: [],
        laundrydata: [],
        transactiondata: [],
        adminkeytemp: '',
        input: {
            fname: '',
            lname: '',
            mi: '',
            position: '',
        }
    },
    getters: {
        //
    },
    actions: {
        unlockadmin({ commit }) {
            axios.post('/api/unlockadmin', {
                status: "unlock",
                completed: false
            }).then((response) => {
                console.log(response)
                commit('setAdmin', response.data)
            })
        },
        lockadmin({ commit }) {
            axios.post('/api/unlockadmin', {
                status: "lock",
                completed: false
            }).then((response) => {
                console.log(response)
                commit('setAdmin', response.data)
            })
        },
        updateKey({ commit }, data) {
            axios.post('/api/updatekey', {
                adminkey: data,
                completed: false
            }).then((response) => {
                commit('setAdmin', response.data)
            })
        },
        fetchAdmin({ commit }) {
            axios.get('/api/getadmin').then((response) => {
                commit('setAdmin', response.data)
            })
        },
        fetchLaundry({ commit }) {
            axios.get('/api/getlaundry').then((response) => {
                console.log(response)
                commit('setLaundry', response.data)
            })
        },
        fetchTransaction({ commit }) {
            axios.get('/api/gettransaction').then((response) => {
                console.log(response)
                commit('setAllTransaction', response.data)
            })
        },
        searchTrans({ commit }, id) {
            axios.post('/api/searchtransaction', {
                cid: id,
                completed: false
            }).then((response) => {
                commit('setResult', response.data)
            })
        },
        addTransaction({ commit }, input) {
            axios.post('/api/addtransaction', {
                id: input.id,
                name: input.name,
                address: input.address,
                phone: input.phone,
                lweight: input.weight,
                total: input.total,
                ammount: input.amount,
                change: input.change,
                completed: false
            }).then((response) => {
                commit('setTransaction', response.data)
            })
        },
        deletetransaction({ commit }, input) {
            axios.post('/api/deletetransaction', {
                id: input,
                completed: false
            }).then((response) => {
                commit('setTransaction', response.data)
            })
        },
        saveprice({ commit }, data) {
            axios.post('/api/saveprice', {
                price: data,
                completed: false
            }).then((response) => {
                commit('setPrice', response.data)
            })
        },
        saveoffer({ commit }, data) {
            axios.post('/api/saveoffer', {
                offers: data,
                completed: false
            }).then((response) => {
                commit('setOffer', response.data)
            })
        },
    },
    mutations: {
        setAdmin: (state, admin) => {
            state.adminkeytemp = admin[0].adminkey;
            state.admindata = admin;
        },
        setTransaction: (state, data) => {
            console.log(data);
            window.location.reload();
        },
        setAllTransaction: (state, data) => {
            console.log(data);
            state.transactiondata = data;
        },
        setLaundry: (state, data) => {
            state.laundrydata = data;
        },
        setPrice: (state, data) => {
            state.laundrydata[1].price = data;
        },
        setOffer: (state, data) => {
            console.log(data);
            window.location.reload();
        },
        setResult: (state, data) => {
            state.searchdata.name = data[0].fullname;
            state.searchdata.weight = data[0].lweight + " Kg";
            state.searchdata.total = "Php " + data[0].total;
            state.searchdata.amount = "Php " + data[0].ammount;
            state.searchdata.change = "Php " + data[0].change;
        },
    },

});