import Vue from 'vue'
import VueRouter from 'vue-router'

import Dashboard from '../../components/pages/index/Dashboard'
import Patron from '../../components/pages/patron/Patron'
import Book from '../../components/pages/books/Books'
import Settings from '../../components/pages/settings/Settings'

//for router
Vue.use(VueRouter)

export const router = new VueRouter({
    routes: [
        { path: '', component: Dashboard },
        { path: '/patron', component: Patron },
        { path: '/book', component: Book },
        { path: '/settings', component: Settings }
    ]
  })
  
