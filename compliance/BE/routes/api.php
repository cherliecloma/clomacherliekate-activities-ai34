<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LaundryDashboardController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\NewTransactionController;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\UpdateTitleController;
use App\Http\Controllers\UpdateKeyController;
use App\Http\Controllers\SearchTransactionController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::resource('/checksenator', CheckSenatorController::class);
Route::resource('/getadmin', AdminController::class)->only([
    'index', 'show'
]);
Route::resource('/unlockadmin', AdminController::class)->only([
    'store', 'post'
]);
Route::resource('/updatetitle', UpdateTitleController::class)->only([
    'store', 'post'
]);
Route::resource('/updatekey', UpdateKeyController::class)->only([
    'store', 'post'
]);

Route::resource('/getlaundry', LaundryDashboardController::class)->only([
    'index', 'show'
]);
Route::resource('/gettransaction', TransactionController::class)->only([
    'index', 'show'
]);
Route::resource('/addtransaction', TransactionController::class)->only([
    'store', 'post'
]);
Route::resource('/deletetransaction', NewTransactionController::class)->only([
    'store', 'post'
]);
Route::resource('/saveprice', PriceController::class)->only([
    'store', 'post'
]);
Route::resource('/saveoffer', OfferController::class)->only([
    'store', 'post'
]);
Route::resource('/searchtransaction', SearchTransactionController::class)->only([
    'store', 'post'
]);





